# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA
# Copyright (C) 2019 Novo Nordisk Foundation Center for Biosustainability,
#     Technical University of Denmark

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
"""Simulate the model with different parameters."""


def test_default_simulation(assembled_dfba):
    """Run the model as is but don't modify fixture."""
    _, _ = assembled_dfba.simulate(0.0, 25.0, 0.1)


def test_control_parameter_simulation(assembled_dfba):
    """Add control parameter and perform the simulation."""
    from dfba import ControlParameter

    Oxy = ControlParameter("Oxygen", [7.7], [8.0, 0.0])
    assembled_dfba.add_exchange_flux_lb("EX_o2(e)", Oxy, control_parameters=Oxy)
    _, _ = assembled_dfba.simulate(0.0, 25.0, 0.1)


def test_default_simulation_with_fluxes(assembled_dfba):
    """Run the model as is but don't modify fixture."""
    _, _ = assembled_dfba.simulate(
        0.0, 25.0, 0.1, ["EX_glc(e)", "EX_xyl_D(e)", "EX_etoh(e)"]
    )


def test_direct_method(assembled_dfba):
    """Change parameters as in example5 and run the model."""
    assembled_dfba.solver_data.set_algorithm("direct")
    assembled_dfba.solver_data.set_display("none")
    assembled_dfba.solver_data.set_ode_method("ADAMS")
    _, _ = assembled_dfba.simulate(0.0, 25.0, 0.1)
