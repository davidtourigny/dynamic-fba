## Examples

This directory contains the following content

* [`./example1.py`](./example1.py): anaerobic growth of *E. coli* on glucose and
  xylose
* [`./example2.py`](./example2.py): aerobic growth of *E. coli* on glucose and
  xylose
* [`./example3.py`](./example3.py): aerobic growth of *S. cerevisiae* on glucose
  with switch to anaerobic conditions at *t=7.7h*
* [`./example4.py`](./example4.py): aerobic growth of *S. cerevisiae* on glucose and
  xylose
* [`./example5.py`](./example5.py): anaerobic growth of *E. coli* on glucose and
  xylose simulated using direct method
* [`./README.md`](./README.md): this document

