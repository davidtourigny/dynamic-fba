======
Citing
======

If this package has contributed to your own work you can use the following citation:

Tourigny DS, Carrasco Muriel J, Beber ME (2020). dfba: Software for efficient simulation of dynamic flux-balance analysis models in Python. `Journal of Open Source Software, 5(52), 2342 <https://doi.org/10.21105/joss.02342>`_
