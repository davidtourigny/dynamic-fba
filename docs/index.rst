.. dfba documentation main file, created by
   sphinx-quickstart on Thu Nov 28 10:05:22 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dfba's documentation!
=======================================

.. image:: https://img.shields.io/pypi/v/dfba.svg
   :target: https://pypi.org/project/dfba/
   :alt: Current PyPI Version

.. image:: https://img.shields.io/pypi/pyversions/dfba.svg
   :target: https://pypi.org/project/dfba/
   :alt: Supported Python Versions

.. image:: https://img.shields.io/pypi/l/dfba.svg
   :target: http://www.gnu.org/licenses/
   :alt: GPLv3+

.. image:: https://gitlab.com/davidtourigny/dynamic-fba/badges/main/pipeline.svg
   :target: https://travis-ci.org/davidtourigny/dynamic-fba/commits/main
   :alt: Pipeline Status

.. image:: https://gitlab.com/davidtourigny/dynamic-fba/badges/main/coverage.svg
   :target: https://gitlab.com/davidtourigny/dynamic-fba/commits/main
   :alt: Coverage Report

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/ambv/black
   :alt: Black

.. image:: https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg
   :target: https://www.contributor-covenant.org/version/1/4/code-of-conduct.html
   :alt: Contributor Covenant

.. image:: https://joss.theoj.org/papers/10.21105/joss.02342/status.svg
   :target: https://doi.org/10.21105/joss.02342

.. _GLPK: https://www.gnu.org/software/glpk/
.. _SUNDIALS: https://computation.llnl.gov/projects/sundials
.. _Python: https://www.python.org/
.. _cobrapy: https://github.com/opencobra/cobrapy
.. _optlang: https://github.com/biosustain/optlang
.. _symengine: https://github.com/symengine/symengine

This project provides an object-oriented software package for dynamic
flux-balance analysis (DFBA) simulations using implementations of the direct
method or Algorithm 1 described in the paper `Harwood et al., 2016 <https://link.springer.com/article/10.1007/s00211-015-0760-3>`_. The main
algorithms for solving embedded LP problems are written in C++ and use the GNU
Linear Programming Kit (GLPK_) and the Suite of Nonlinear and
Differential/Algebraic Equation Solvers (SUNDIALS_) CVODE or IDA. Extension
modules to cobrapy_ are provided for easy generation and simulation of DFBA
models.

Overview
========

.. _`pybind11`: https://github.com/pybind/pybind11
.. _examples: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/examples

This software package implements the fastest and most robust DFBA simulation algorithms in a compiled programming language that is made accessible to users through a simple and intuitive interface with cobrapy_. The target audience is researchers wanting to build and efficiently simulate specific DFBA models of interest, but not spend their time wrestling with the numerical challenges of DFBA. Users are not expected to interact directly with the lower-level *C++* interface
and once installed the package should ideally remain untouched. Instead, the
classes and functions for solving embedded LP problems have been exposed to
Python_ using `pybind11`_. Combined with the provided cobrapy_ extension
modules, this provides the user with the ability to build their own DFBA model
exclusively in Python_.  

The Python_ class :class:`dfba.DfbaModel` intuitively encapsulates
all the data required for a full definition of a DFBA model by combining an
underlying cobrapy_ object with instances of the :class:`dfba.KineticVariable` and
:class:`dfba.ExchangeFlux` classes. The :class:`dfba.DfbaModel` class instance ensures all user data are
consistent with the initialization and simulation requirements of an embedded LP
problem. User data are passed directly to the algorithms and symbolic functions
are dynamically compiled and loaded prior to simulation.  

The directory examples_ also contains scripts for the examples described in the :doc:`examples` section,
and details on how the user can adapt these to build and simulate their own model are outlined in the section :doc:`example1`.

.. toctree::
   :maxdepth: 2
   :hidden:

   installation.rst
   plotting.rst
   examples.rst
   example1.ipynb
   example3.ipynb
   example5.ipynb
   contributing.rst
   citing.rst
   source_files.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
