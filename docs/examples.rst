Example DFBA models
===================

.. _example1: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/examples/example1.py
.. _example2: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/examples/example2.py
.. _example3: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/examples/example3.py
.. _example4: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/examples/example4.py
.. _example5: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/examples/example5.py

The current version is distributed with several examples related to Examples
6.2.1 and 6.3 in `Harwood et al., 2016 <https://link.springer.com/article/10.1007/s00211-015-0760-3>`_.

Examples example1_ and example2_ are based on `Hanly & Henson, 2011 <https://onlinelibrary.wiley.com/doi/abs/10.1002/bit.22954>`_ and also Example 1 in
`DFBAlab <https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0409-8>`_.
Example example5_ implements the same model as example1_,
but uses the direct method in place of Algorithm 1 from
`Harwood et al., 2016 <https://link.springer.com/article/10.1007/s00211-015-0760-3>`_. Conversely, example3_ and example4_ are based on
`Hanly & Henson, 2011 <https://onlinelibrary.wiley.com/doi/abs/10.1002/bit.22954>`_.

Genome-scale Metabolic Networks
-------------------------------

.. _sbml-models: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/sbml-models

The genome-scale metabolic models used in these examples are provided in the directory sbml-models_

* `iJR904 <http://bigg.ucsd.edu/models/iJR904>`_: *Escherichia coli* bacterium
  iJR904 contains 761 metabolites and 1075 reaction fluxes.
* `iND750 <http://bigg.ucsd.edu/models/iND750/>`_: *Saccharomyces cerevisiae*
  strain S288C iND750 contains 1059 metabolites and 1266 reaction fluxes.
