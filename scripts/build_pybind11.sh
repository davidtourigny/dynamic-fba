#!/usr/bin/env bash

# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA
# Copyright (C) 2019 Novo Nordisk Foundation Center for Biosustainability,
#     Technical University of Denmark

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -eux

# Use a default of one core.
: "${NPROC:=1}"

# Download pybind11 master branch.
curl -L -O "https://github.com/pybind/pybind11/archive/master.tar.gz"

# Unpack and install pybind11.
tar -xzf "master.tar.gz"
cd "pybind11-master"
mkdir build
cd build
cmake ..
make install -j "${NPROC}"
cd ../..
rm -rf "pybind11-master" "master.tar.gz"
